package src.memento;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface MementoUser {
    /**
     * Save previous panel name.
     *
     * @param panelName the panel name
     */
    void savePreviousPanelName(String panelName);
    /**
     * Gets the previous panel name.
     *
     * @return the previous panel name
     */
    String getPreviousPanelName();
}
