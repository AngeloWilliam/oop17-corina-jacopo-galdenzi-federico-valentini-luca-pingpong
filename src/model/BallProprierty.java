package src.model;

import java.awt.Dimension;
import java.awt.Point;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import src.utilities.Direction;
import src.utilities.Utility;

import java.util.List;
/**
 * The Class BallProprierties.
 * This class rappresents the ball's proprierties such as:
 * position of the ball, the speed, the diameter, the direction, visibility.
 * It allows you to handle the characteristics of the ball.
 */
public class BallProprierty {
    private final Point position;
    private final Point originalPosition;
    private final List<Integer> speed;
    private final Boundary boundaries;
    private int diameter;
    private Direction directionXBall;
    private Direction directionYBall;
    private boolean visibility;
    private static final int X = 0;
    private static final int Y = 1;
    /**
     * Instantiates a new ball proprierty, it instantiates also the original position and the position and the speed.
     * @param sizeWindow the dimension of the window.
     * @param diameter the diameter of the ball.
     */
    public BallProprierty(final Dimension sizeWindow, final int diameter) {
        this.diameter = diameter;
        this.boundaries = new Boundary(sizeWindow);
        this.position = new Point((int) (boundaries.getXBoundLaneRight() / 2) - (this.diameter / 2), (int) (boundaries.getYBoundLaneDown() / 2) - (this.diameter / 2));
        this.originalPosition = new Point(this.position);
        this.speed = IntStream.rangeClosed(1, 2).boxed().collect(Collectors.toList());
        this.visibility = true;
        randomizeSpeed();
        randomizeDirection();
    }

    /**
     * Gets the speed with the right sign in according to the direction of the ball.
     *
     * @return the speed of the X value.
     */
    public int getSpeedWithSignX() {
        if ((this.directionXBall == Direction.LEFT && speed.get(X) > 0) || (this.directionXBall == Direction.RIGHT && speed.get(X) < 0)) {
            return this.speed.get(X) * (-1);
        }
        return this.speed.get(X);
    }
    /**
     * Gets the speed with the right sign in according to the direction of the ball.
     *
     * @return the speed of the Y value.
     */
    public int getSpeedWithSignY() {
        if ((this.directionYBall == Direction.UP && this.speed.get(Y) > 0) || (this.directionYBall == Direction.DOWN && speed.get(Y) < 0)) {
            return this.speed.get(Y) * (-1);
        }
        return this.speed.get(Y);
    }
    /**
     * Add to the current speed the value you pass as parameter.
     * @param   x       the x value to add to the current speed.
     * @param   y       the y value to add to the current speed.
     */
    public void addToSpeed(final int x, final int y) {
        this.speed.set(X, this.speed.get(X) + x);
        this.speed.set(Y, this.speed.get(Y) + y);
    }
    /**
     * Set the X speed.
     * @param x the value to set to the X speed.
     */
    public void setSpeedX(final int x) {
        this.speed.set(X, x);
    }
    /**
     * Set the Y speed.
     * @param y the value to set to the Y speed.
     */
    public void setSpeedY(final int y) {
        this.speed.set(Y, y);
    }
    /**
     * Gets the diameter of the ball.
     * @return the diameter of the ball.
     */
    public int getDiameter() {
        return this.diameter;
    }
    /**
     * Sets the diameter of the ball.
     * @param diameter the diameter of the ball
     */
    public void setDiameter(final int diameter) {
        this.diameter = diameter;
    }
    /**
     * Gets the Boundary class of the field.
     * @return the Boundary class of the field
     */
    public Boundary getBoundaryWindow() {
        return this.boundaries;
    }
    /**
     * This method randomize the direction of the ball when someone has score or at game's start.
     */
    public void randomizeDirection() {
        if (Utility.getRandomNumberInRange(1, 10) % 2 == 0) {
            this.directionXBall = Direction.LEFT;
        } else {
            this.directionXBall = Direction.RIGHT; 
        }
        if (Utility.getRandomNumberInRange(1, 10) % 2 == 0) {
            this.directionYBall = Direction.UP;
        } else {
            this.directionYBall = Direction.DOWN; 
        }
    }
    /**
     * Sets the speed with the values passed as parameters.
     * @param x the x speed to be setted
     * @param y the y speed to be setted
     */
    private void setSpeed(final int x, final int y) {
        this.setSpeedX(x);
        this.setSpeedY(y);
    }
    /**
     * Randomize speed.
     */
    public void randomizeSpeed() {
        setSpeed(Utility.getRandomNumberInRange(GameConstant.BALL_MIN_SPEED_X, 
                GameConstant.BALL_MAX_SPEED_X), Utility.getRandomNumberInRange(GameConstant.BALL_MIN_SPEED_Y, 
                        GameConstant.BALL_MAX_SPEED_Y));
    }
    /**
     * Restore the initial speed of the ball.
     */
    public void restoreInitialSpeed() {
        this.setSpeed(GameConstant.BALL_MEDIUM_SPEED_X, GameConstant.MEDIUM_ANGLE_BALL);
    }
    /**
     * Make ball visible.
     */
    public void makeBallVisible() {
        this.visibility = true;
    }
    /**
     * Make ball invisible.
     */
    public void makeBallInvisible() {
        this.visibility = false;
    }
    /**
     * Gets the visibility of the ball.
     *
     * @return the visibility of the ball
     */
    public boolean getVisibility() {
        return visibility;
    }
    /**
     * Gets the X direction of the ball.
     * @return X direction
     */
    public Direction getXDirection() {
        return this.directionXBall;
    }
    /**
     * Gets the Y direction of the ball.
     * @return Y direction
     */
    public Direction getYDirection() {
        return this.directionYBall;
    }
    /**
     * Change the X direction of the ball, this method allow you to reverse the X direction.
     */
    public void changeXDirection() {
        if (directionXBall == Direction.LEFT) {
            directionXBall = Direction.RIGHT;
        } else {
            directionXBall = Direction.LEFT;
        }
    }
    /**
     * Change the Y direction of the ball, this method allow you to reverse the X direction.
     */
    public void changeYDirection() {
        if (directionYBall == Direction.UP) {
            directionYBall = Direction.DOWN;
        } else {
            directionYBall = Direction.UP;
        }
    }
    /**
     * Update the current position of the ball with the values passed as parameters.
     * @param x the x position to be added to the current position.
     * @param y the y position to be added to the current position.
     */
    public void addToPosition(final int x, final int y) {
        this.position.setLocation(this.position.x + x, this.position.y + y);
    }
    /**
     * Gets the X position of the ball.
     * @return X position of the ball.
     */
    public int getX() {
        return (int) this.position.getX();
    }
    /**
     * Gets the Y position of the ball.
     * @return Y position of the ball.
     */
    public int getY() {
        return (int) this.position.getY();
    }
    /**
     * Replace the X position of the ball with the value passed as parameter.
     * @param x the new position to set.
     */
    public void setX(final int x) {
        this.position.setLocation(x, this.position.y);
    }
    /**
     * Replace the Y position of the ball with the value passed as parameter.
     * @param y the new position to set.
     */
    public void setY(final int y) {
        this.position.setLocation(this.position.x, y);
    }
    /**
     * Restore the original position of the ball.
     */
    public void restoreOriginalPosition() {
        this.randomizeDirection();
        this.position.setLocation(this.originalPosition);
    }
}
