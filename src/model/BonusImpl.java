package src.model;

import java.awt.Point;

import src.utilities.BonusType;

/**
 * 
 * @author Federico Galdenzi
 * This class represents the characteristics of the Bonus such as position, type and diameter.
 */
public class BonusImpl implements Bonus {
    private Point position;
    private BonusType type;
    private int diameter;
    /**
     * Instantiates the BonusImpl class with the position, type and diameter.
     * @param p is the position of the bonus
     * @param t is the type of the bonus
     * @param d is the diameter of the bonus
     */
    public BonusImpl(final Point p, final BonusType t, final int d) {
        this.position = p;
        this.type = t;
        this.diameter = d;
    }
    /**
     * Instantiates the BonusImpl class with only the position.
     * @param p the position
     */
    public BonusImpl(final Point p) {
        this.position = p;
    }
    @Override
    public final Point getPosition() {
        return this.position;
    }
    @Override
    public final BonusType getType() {
        return this.type;
    }
    @Override
    public final int getDiameter() {
        return this.diameter;
    }
    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((position == null) ? 0 : position.hashCode());
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof BonusImpl)) {
            return false;
        }
        BonusImpl other = (BonusImpl) obj;
        if (position == null) {
            if (other.position != null) {
                return false;
            }
        } else if (!position.equals(other.position)) {
            return false;
        }
        return true;
    }
}
