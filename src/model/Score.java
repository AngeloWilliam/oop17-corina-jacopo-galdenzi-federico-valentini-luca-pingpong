package src.model;

import java.io.Serializable;

/**
 * The Interface Score, manage the Player's score.
 *
 * @author Jacopo Corina
 */
public interface Score extends Serializable {

    /**
     * Increments the player1's score.
     */
    void addScorePlayer1();

    /**
     * Increments the player2's score.
     */
    void addScorePlayer2();

    /**
     * Reset the score.
     */
    void reset();

    /**
     * Gets the winner.
     *
     * @return the winner
     */
    int getWinner();

    /**
     * Gets the winner which has reached the setting maximum score.
     *
     * @return the winner with maximum
     */
    int getWinnerWithMaximum();

    /**
     * Gets the player1's score.
     *
     * @return the player1's score.
     */
    int getScorePlayer1();

     /**
     * Gets the player2's score.
     *
     * @return the player2's score
     */
    int getScorePlayer2();

    /**
     * Sets the player1 name.
     *
     * @param s the new player1 name
     */
    void setPlayer1Name(String s);

    /**
     * Sets the player2 name.
     *
     * @param s the new player2 name
     */
    void setPlayer2Name(String s);

    /**
     * Gets the player1 name.
     *
     * @return the player1 name
     */
    String getPlayer1Name();

    /**
     * Gets the player2 name.
     *
     * @return the player2 name
     */
    String getPlayer2Name();
}
