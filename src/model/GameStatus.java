package src.model;
import src.memento.MementoUser;
import src.observerpattern.ExpiringMatchTimerObserver;
import src.observerpattern.TimerObserver;

/**
 * 
 * @author Jacopo Corina
 *
 */
public interface GameStatus extends MementoUser {
    /**
     * Sets the timer value.
     *
     * @param timerValue the new timer value
     */
    void setTimerValue(int timerValue);

    /**
     * Sets the maximum score.
     *
     * @param max the new maximum score
     */
    void setMaximumScore(int max);
    /**
     * Sets the player1 name.
     *
     * @param s the new player1 name
     */
    void setPlayer1Name(String s);
    /**
     * Sets the player2 name.
     *
     * @param s the new player2 name
     */
    void setPlayer2Name(String s);
    /**
     * Adds the score player1.
     */
    void addScorePlayer1();
    /**
     * Adds the score player2.
     */
    void addScorePlayer2();
    /**
     * Gets the score1.
     *
     * @return the score1
     */
    int getScore1();
    /**
     * Gets the score 2.
     *
     * @return the score 2
     */
    int getScore2();
    /**
     * Reset score.
     */
    void resetScore();
    /**
     * Start match.
     */
    void startMatch();
    /**
     * Pause match timer.
     */
    void pauseMatchTimer();
    /**
     * Resume match timer.
     */
    void resumeMatchTimer();
    /**
     * Reset match timer.
     */
    void resetMatchTimer();
    /**
     * Gets the winner in score mode.
     *
     * @return the winner score mode
     */
    int getWinnerScoreMode();
    /**
     * Gets the winner in timer mode.
     *
     * @return the winner timer mode
     */
    int getWinnerTimerMode();
    /**
     * Gets the score.
     *
     * @return the score
     */
   Score getScore();
   /**
    * @param obs a timer observer
    */
   void passObserverToTimer(TimerObserver obs);
   /**
    * 
    * @param obs an expiring match timer observer
    */
   void passObserverToExpiringTimer(ExpiringMatchTimerObserver obs);
}


