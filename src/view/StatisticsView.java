
package src.view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import src.model.Score;
import src.resources.ResourcesManagement;

/**
 * 
 * @author Jacopo Corina
 * This class represent the StatisticView panel.
 */
public class StatisticsView extends JPanel {
    private static final long serialVersionUID = 3981656308122611782L;
    private JTable table;
    private DefaultTableModel model;
    private JScrollPane scrollPane;
    private static final int CONSTANT_HEIGHT_DEFAULT = 800;
    /**
     * this is the constructor method of StatisticsView class.
     */
    public StatisticsView() {
        this.setLayout(new GridLayout(1, 1));
    }

    /**
     * Fills a table with the match score history.
     */
    public void fillTableModel() {
        List<Score> l = ResourcesManagement.getScoreHistory();
        if (l != null) {
            List<Object[]> l2 = l.stream()
                    .map(e -> new Object[] { e.getPlayer1Name(), e.getScorePlayer1(), e.getPlayer2Name(), e.getScorePlayer2() })
                    .collect(Collectors.toList());
            l2.iterator().forEachRemaining(e -> {
                model.addRow(e);
            });
        }
    }

    /**
     * this method set the columnsName.
     */
    public void setColumnsName() {
        model = new MyModel();
        model.addColumn(model.getColumnName(0));
        model.addColumn(model.getColumnName(1));
        model.addColumn(model.getColumnName(2));
        model.addColumn(model.getColumnName(3));
    }

    /**
     * Creates table graphics with the model previously filled.
     */
    public void applyFilling() {
        table = new JTable(model);
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sh = (int) screen.getHeight();
        table.setRowHeight(table.getRowHeight() * sh / StatisticsView.CONSTANT_HEIGHT_DEFAULT);
        scrollPane = new JScrollPane(table);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        this.add(scrollPane);
    }

    /**
     * Delete table graphics.
     */
    public void removeTable() {
        this.remove(scrollPane);
    }

    private class MyModel extends DefaultTableModel {
        private static final long serialVersionUID = -880708134290353024L;
        private String[] names = { "Player1 name", "Player1 score", "Player2 name", "Player2 score" };

        @Override
        public boolean isCellEditable(final int row, final int col) {
            return false;
        }

        @Override
        public String getColumnName(final int index) {
            return names[index];
        }
    }

}
