package src.view.template;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.RoundRectangle2D;
import java.util.Optional;
import javax.swing.JButton;
/**
 * 
 * @author Internet
 * this class is a customized JButton
 */
public class RoundedCornerButton extends JButton {
    private static final long serialVersionUID = 4970857045165243579L;
    private static final double ARC_WIDTH  = 16d;
    private static final double ARC_HEIGHT = 16d;
    private static final int FOCUS_STROKE = 2;
    private final Color fc = new Color(100, 150, 255, 200);
    private final Color ac = new Color(230, 230, 230);
    private final Color bc = new Color(250, 250, 250);
    private final Color rc = Color.BLUE;
    private Shape shape;
    private Shape border;
    private Shape base;
    /**
     * 
     * @param text this is the text of the button.
     */
    public RoundedCornerButton(final String text) {
        super(text);
    }
    @Override
    public final void updateUI() {
        super.updateUI();
        setContentAreaFilled(false);
        setFocusPainted(false);
        setBackground(bc);
        initShape();
    }

    /**
     * Bounds creation.
     */
    protected final void initShape() {
        if (!getBounds().equals(base)) {
            base = getBounds();
            shape = new RoundRectangle2D.Double(0, 0, getWidth() - 1, getHeight() - 1, ARC_WIDTH, ARC_HEIGHT);
            border = new RoundRectangle2D.Double(FOCUS_STROKE, FOCUS_STROKE,
                    getWidth() - 1 - FOCUS_STROKE * 2,
                    getHeight() - 1 - FOCUS_STROKE * 2,
                    ARC_WIDTH, ARC_HEIGHT);
        }
    }
    private void paintFocusAndRollover(final Graphics2D g2, final Color color) {
        g2.setPaint(new GradientPaint(0, 0, color, getWidth() - 1, getHeight() - 1, color.brighter(), true));
        g2.fill(shape);
        g2.setPaint(getBackground());
        g2.fill(border);
    }

    @Override
    protected final void paintComponent(final Graphics g) {
        initShape();
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        if (getModel().isArmed()) {
            g2.setPaint(ac);
            g2.fill(shape);
        } else if (isRolloverEnabled() && getModel().isRollover()) {
            paintFocusAndRollover(g2, rc); /*Border color*/
        } else if (hasFocus()) {
            paintFocusAndRollover(g2, fc);
        } else {
            g2.setPaint(getBackground());
            g2.fill(shape);
        }
        g2.dispose();
        super.paintComponent(g);
    }
    @Override
    protected final void paintBorder(final Graphics g) {
        initShape();
        Graphics2D g2 = (Graphics2D) g.create();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(getForeground());
        g2.draw(shape);
        g2.dispose();
    }
    @Override
    public final boolean contains(final int x, final int y) {
        initShape();
        //return shape != null && shape.contains(x, y);
        //return Optional.ofNullable(shape).filter(s -> s.contains(x, y)).isPresent();
        return Optional.ofNullable(shape).map(s -> s.contains(x, y)).orElse(false);
    }
}
