package src.view;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.Optional;
import javax.swing.JButton;
import javax.swing.JPanel;

import src.resources.ResourcesManagement;
import src.utilities.Utility;
import src.view.template.RoundedCornerButton;

/**
 * 
 * @author Jacopo Corina This class represent the Panel of the StartView class.
 */
public class StartView extends JPanel {
    private static final long serialVersionUID = -8919731045178911543L;
    private JButton playButton;
    private JButton statisticsButton;
    // private JButton creditsButton;
    private JButton optionsButton;
    private static final int CONSTANT_SPACE_AMONG_BUTTONS = 50;
    /**
     * this is the constructor method of StartView class.
     */
    public StartView() {
        int spaceAmongButtons = StartView.CONSTANT_SPACE_AMONG_BUTTONS;
        Optional<Font> appFont = Optional.ofNullable(Utility.fontLoader(ResourcesManagement.getCustomFontStream()));
        this.setLayout(new GridLayout(3, 1, 0, spaceAmongButtons));
        playButton = new RoundedCornerButton("Start the match");
        statisticsButton = new RoundedCornerButton("Scores");
        // creditsButton = new RoundedCornerButton("Credits");
        optionsButton = new RoundedCornerButton("Options");
        this.add(playButton);
        this.add(statisticsButton);
        this.add(optionsButton);
        // this.add(creditsButton);
        appFont.ifPresent(x -> {
            playButton.setFont(x);
            statisticsButton.setFont(x);
            // creditsButton.setFont(x);
            optionsButton.setFont(x);
        });
    }
    /**
     * this method add to the playButton object the lister you pass as parameter.
     * 
     * @param l
     *            an action listener invoked on play click
     */
    public void addPlayButtonListener(final ActionListener l) {
        playButton.addActionListener(l);
    }
    /**
     * this method add to the statisticsButton object the lister you pass as
     * parameter.
     * 
     * @param l
     *            an action listener invoked on statistics click
     */
    public void addStatisticsButtonListener(final ActionListener l) {
        statisticsButton.addActionListener(l);
    }
    /**
     * this method add to the optionsButton object the lister you pass as parameter.
     * 
     * @param l
     *            an action listener invoked on options click
     */
    public void addOptionsButtonListener(final ActionListener l) {
        optionsButton.addActionListener(l);
    }
}
