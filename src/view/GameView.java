package src.view;

import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 * 
 * @author Jacopo Corina
 *
 */
public class GameView extends JPanel {

    private static final long serialVersionUID = 808562911658421239L;
    private ScoreZone scoreZone;
    private MatchZone matchZone;
    private int borderTickness = 10;

    /**
     * Creates the game view, divided in two zones : one for the score and the time,
     * the other one for the match.
     */
    public GameView() {
        scoreZone = new ScoreZone();
        matchZone = new MatchZone(scoreZone.getScore(), scoreZone.getTime(), scoreZone.getHitBonusDescription());
        matchZone.setBorder(BorderFactory.createEmptyBorder(0, borderTickness, borderTickness, borderTickness));
        this.setLayout(new BorderLayout());
        this.add(scoreZone, BorderLayout.NORTH);
        this.add(matchZone, BorderLayout.CENTER);

    }

    /**
     * 
     * @return the match zone
     */
    public final MatchZone getMatchZone() {
        return matchZone;
    }
}
