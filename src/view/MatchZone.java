package src.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import src.model.Bonus;
import src.model.GameConstant;
import src.model.OptionsValues;
import src.observerpattern.GraphicBonusShowObserver;
import src.utilities.BonusType;

/**
 * 
 * @author Jacopo Corina
 *
 */
public class MatchZone extends JPanel implements GraphicBonusShowObserver {

    private static final long serialVersionUID = 5264834999364124998L;
    private JLabel time;
    private JLabel score;
    private JLabel bonusDescription;
    private Rectangle racket1;
    private BallView ball;
    private boolean start = true;
    private Rectangle racket2;
    private OptionsValues opt;
    private List<BonusView> bonusList;
    private boolean isBallVisible = true;

    /**
     * 
     * @param score the label indicating the actual match score
     * 
     * @param time the label indicating the remaining match time
     * 
     * @param bonusDescription the label indicating a bonus description (when happens a collision with a bonus)
     * 
     */
    public MatchZone(final JLabel score, final JLabel time, final JLabel bonusDescription) {
        this.score = score;
        this.time = time;
        this.bonusDescription = bonusDescription;
        this.ball = new BallView(this);
        this.bonusList = new ArrayList<>();
    }

    @Override
    public final void paintComponent(final Graphics g) {
        this.setFocusable(true);
        this.requestFocus();

        Graphics2D g2 = (Graphics2D) g;
        super.paintComponent(g2);

        if (start) {
            this.racket1 = new Rectangle(GameConstant.RACKET_ONE_POSITION_X, GameConstant.RACKET_POSITION_Y,
                    GameConstant.RACKET_WIDTH, GameConstant.RACKET_HEIGHT);
            start = false;
            this.racket2 = new Rectangle(GameConstant.RACKET_TWO_POSITION_X, GameConstant.RACKET_POSITION_Y,
                    GameConstant.RACKET_WIDTH, GameConstant.RACKET_HEIGHT);
        }
        bonusList.forEach(e -> {
            switch (e.getType()) {
            case SPEED:
                g2.setColor(Color.GREEN);
                break;
            case BLINK:
                g2.setColor(Color.MAGENTA);
                break;
            case SIZE:
                g2.setColor(Color.CYAN);
                break;
            default:
                break;
            }
            g2.fill(e.getInstance());
        });
        g2.setColor(opt.getRacket1Color());
        g2.fill(racket1);
        g2.setColor(opt.getRacket2Color());
        g2.fill(racket2);
        g2.setColor(opt.getBallColor());
        if (isBallVisible) {
            g2.fill(this.ball.getInstance());
        }
        setBackground(opt.getBackgroundColor());

        if (opt.getMaxScore() == GameConstant.VALUE_NOT_SET) {
            time.setVisible(true);
        } else {
            time.setVisible(false);
        }
    }

    /**
     * get the second racket.
     * 
     * @return the second racket
     */
    public final Rectangle getRacket2() {
        return racket2;
    }

    /**
     * add a player listener that allows to move the player.
     * 
     * @param l is a keyListener
     * 
     */
    public final void addMovePlayerListener(final KeyListener l) {
        this.addKeyListener(l);
    }

    /**
     * set location of the rectangle around the screen.
     * 
     * @param p is a point where the racket will be
     * 
     * @param pl indicates which racket will move
     * 
     */
    public final void movePlayer(final Point p, final int pl) {
        if (pl == 1) {
            this.racket1.setLocation(p);
        } else if (pl == 2) {
            this.racket2.setLocation(p);
        }
        refresh();
        Toolkit.getDefaultToolkit().sync();
    }

    /**
     * get the first racket.
     * 
     * @return the first racket
     */
    public final Rectangle getRacket1() {
        return racket1;
    }
    /**
     * 
     * @param n remaining match time
     */
    public final void showRemainingTime(final int n) {
        SwingUtilities.invokeLater(() -> {
            time.setText("Remaining time: " + String.valueOf(n));
        });
    }
    /**
     * 
     * @param p1 score 1
     * @param p2 score 2
     */
    public final void setScore(final int p1, final int p2) {
        score.setText(String.valueOf(p1) + " - " + String.valueOf(p2));
    }
    /**
     * Refreshes ball position.
     */
    public final void moveBall() {
        refresh();
        Toolkit.getDefaultToolkit().sync();

    }

    private void refresh() {
        repaint();
        Toolkit.getDefaultToolkit().sync();

    }
    /**
     * 
     */
    public final void initializeMatchGraphics() {
        start = true;
        refresh();
    }
    /**
     * 
     * @return the ball graphics
     */
    public final BallView getBall() {
        return this.ball;
    }
    /**
     * 
     * @param opt option values to apply
     */
    public final void setOptionsValues(final OptionsValues opt) {
        this.opt = opt;
    }
    /**
     * 
     * @return 0 if the first button is selected,1 for the second, otherwise a negative value
     */
    public final int showGameModeBox() {
        MessageBoxGameMode myPanel = new MessageBoxGameMode();
        int result = JOptionPane.showConfirmDialog(null, myPanel, "Choose game mode", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            return myPanel.getSelected();
        } else {
            return GameConstant.VALUE_NOT_SET;
        }
    }
    /**
     * 
     * @param playerNumber player number
     * @return the name written, if the box is closed or is not filled, the default player name is returned
     */
    public final String showPlayerNameBox(final int playerNumber) {
        String name = JOptionPane.showInputDialog("Choose Player " + playerNumber + " name:");
        if (name == null || name.equals("")) {
            if (playerNumber == 1) {
                name = GameConstant.DEFAULT_P1_NAME;
            } else if (playerNumber == 2) {
                name = GameConstant.DEFAULT_P2_NAME;
            } else {
                name = "";
            }

        }
        return name;

    }
    /**
     * 
     * @return 0 if the first is selected, 1 for the second, a negative value otherwise
     */
    public final int showDifficultyBox() {
        MessageBoxAIDifficulty myPanel = new MessageBoxAIDifficulty();
        int result = JOptionPane.showConfirmDialog(null, myPanel, "Choose COM skills", JOptionPane.DEFAULT_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            return myPanel.getSelected();
        } else {
            return GameConstant.VALUE_NOT_SET;
        }
    }
    /**
     * 
     * @param winner the winner number
     */
    public final void showWinner(final int winner) {
        String s;
        if (winner == 0) {
            s = "Parity";
        } else {
            s = "Player " + winner + " wins";
        }
        JOptionPane.showMessageDialog(null, s);
    }

    @Override
    public final void removeBonus(final Point bonusPosition) {
        SwingUtilities.invokeLater(() -> {
            bonusList.remove(new BonusView(bonusPosition));
            refresh();
        });

    }

    @Override
    public final void addBonus(final Bonus bonus) {
        SwingUtilities.invokeLater(() -> {
            bonusList.add(new BonusView(bonus.getPosition(), bonus.getType()));
        });
    }

    @Override
    public final void clearBonus() {
        SwingUtilities.invokeLater(() -> {
            bonusList.clear();
        });
    }

    @Override
    public final void applyBallVisibility(final boolean state) {
        SwingUtilities.invokeLater(() -> {
            this.isBallVisible = state;
        });
    }

    @Override
    public final void showHitBonusDescription(final BonusType bt) {
        SwingUtilities.invokeLater(() -> {
            if (bt == BonusType.BLINK) {
                bonusDescription
                        .setText(GameConstant.BLINKING_BONUS_TIMER_DURATION + " seconds blinking bonus applied");
            } else if (bt == BonusType.SPEED) {
                bonusDescription.setText(GameConstant.SPEED_BONUS_TIMER_DURATION + " seconds speed bonus applied");
            } else if (bt == BonusType.SIZE) {
                bonusDescription.setText(GameConstant.SIZE_BONUS_TIMER_DURATION + " seconds size bonus applied");
            } else {
                bonusDescription.setText("");
            }
        });
    }

}
