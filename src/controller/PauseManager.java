package src.controller;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface PauseManager {
    /**
     * 
     */
   void quit();
   /**
    * 
    */
   void pause();
   /**
    * 
    */
   void resume();
   /**
    * 
    */
   void invokePauseMenu();
}
