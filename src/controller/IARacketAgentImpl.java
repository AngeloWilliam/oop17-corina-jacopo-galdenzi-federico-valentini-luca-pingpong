package src.controller;

import java.awt.Point;

import src.model.Ball;
import src.model.Racket;
import src.utilities.Direction;
import src.view.ViewManager;

/**
 * this class represent the racket agent and allows to move racket in the game field.
 * 
 * @author Luca Valentini
 */
public class IARacketAgentImpl implements IARacketAgent, ThreadManagement {
    private Ball b;
    private Racket player2;
    private ViewManager theView;
    private Direction lastDirection;
    private int waitingTime;
    private boolean stopped;
    private boolean suspended;

    /**
     * it is the constructor method.
     * 
     * @param ballModel the ballModel.
     * @param player2 is the racket controlled by the cpu
     * @param v represents the view of the game
     * @param waitingTime is used to fall asleep the racket
     */
    public IARacketAgentImpl(final Ball ballModel, final Racket player2, final ViewManager v, final int waitingTime) {
        this.b = ballModel;
        this.player2 = player2;
        this.theView = v;
        this.lastDirection = Direction.UP;
        this.waitingTime = waitingTime;
        this.stopped = false;
        this.suspended = false;
    }

    @Override
    public final void run() {
        Point p = null;
        while (!stopped) {
            try {
                if (b.getProprierty().getYDirection() == Direction.DOWN && Direction.DOWN == this.lastDirection
                        && b.getProprierty().getY() - ((player2.getYDown() + player2.getYUp()) / 2) < 0) {
                } else if (b.getProprierty().getYDirection() == Direction.UP && Direction.UP == this.lastDirection
                        && b.getProprierty().getY() - ((player2.getYDown() + player2.getYUp()) / 2) > 0) {
                } else if (b.getProprierty().getY() - ((player2.getYDown() + player2.getYUp()) / 2) > 0) {
                    p = player2.move(Direction.DOWN);
                    this.lastDirection = Direction.DOWN;
                    theView.getMatchZone().movePlayer(p, 2);
                } else if (b.getProprierty().getY() - ((player2.getYDown() + player2.getYUp()) / 2) < 0) {
                    p = player2.move(Direction.UP);
                    this.lastDirection = Direction.UP;
                    theView.getMatchZone().movePlayer(p, 2);
                }
                p = null;
                Thread.sleep(waitingTime);
                synchronized (this) {
                    if (suspended) {
                        wait();
                    }
                }
            } catch (java.lang.NullPointerException e) {
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public final synchronized void resumeThread() {
        suspended = false;
        notify();
    }

    @Override
    public final void pauseThread() {
        suspended = true;
    }

    @Override
    public final synchronized void stopThread() {
        this.stopped = true;
        notify();
    }
}
