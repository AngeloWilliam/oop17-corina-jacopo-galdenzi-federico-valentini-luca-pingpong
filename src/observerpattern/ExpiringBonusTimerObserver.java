package src.observerpattern;

import src.model.BonusTimer;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ExpiringBonusTimerObserver {
    /**
     * 
     * @param bs the bonus strategy to apply when a bonus expires
     */
    void executeOnBonusTimerExpiration(BonusTimer bs);
}
