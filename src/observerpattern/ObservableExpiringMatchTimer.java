package src.observerpattern;
/**
 * 
 * @author Jacopo Corina
 *
 */
public interface ObservableExpiringMatchTimer {
    /**
     * 
     * @param observer the observer which will be notified when the match timer expires
     */
    void setExpiringMatchTimerObserver(ExpiringMatchTimerObserver observer);
    /**
     *  notifies the observer when the match timer expires.
     */
    void notifyExpiringMatchTimerObserver();
}
