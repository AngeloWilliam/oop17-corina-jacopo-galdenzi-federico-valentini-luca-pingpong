package src.resources;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import src.model.Score;
/**
 * 
 * @author Jacopo Corina
 * This class allows to manage resources(audio files,ecc...).
 *
 */
public final class ResourcesManagement {
    private static boolean soundOn;;
    private ResourcesManagement() {
        soundOn = false;
    }

    private static final String CUSTOM_FONT = "Capture_it.ttf";
    private static final String MENU_BUTTONS_SOUND = "menuButtons.wav";
    private static final String PLAYER_GOAL_SOUND = "playerScore.wav";
    private static final String RACKET_HIT_SOUND = "racketHit.wav";
    private static final String BORDER_HIT_SOUND = "borderHit.wav";
    private static final String BONUS_HIT_SOUND = "bonusHit.wav";
    private static final String FILE_SEPARATOR = "/";
    private static final String HOME_DIRECTORY = System.getProperty("user.home");
    private static final String MATCH_SCORES = "matchScores.obj";
    /**
     * This method switches the sound effects status. It will be enabled if it's disabled and viceversa
     */
    public static void switchSound() {
        if (!soundOn) {
            soundOn = true;
        } else {
            soundOn = false; 
        }
    }
    private static InputStream buildResourceStream(final String str) {

        InputStream s = ResourcesManagement.class.getClassLoader().getResourceAsStream("resources" + FILE_SEPARATOR + str);
        return s;
    }
    /**
     * 
     * @return The input stream of the choosen font
     */
    public static InputStream getCustomFontStream() {
        return buildResourceStream(CUSTOM_FONT);
    }
    /**
     * 
     * @return The input stream of the button click sound
     */
    public static InputStream getMenuButtonSoundStream() {
        return buildResourceStream(MENU_BUTTONS_SOUND);
    }
    /**
     * 
     * @return The input stream of the racket hit sound
     */
    public static InputStream getRacketHitSoundStream() {
        return buildResourceStream(RACKET_HIT_SOUND);
    }
    /**
     * 
     * @return The input stream of the border hit sound
     */
    public static InputStream getBorderHitSoundStream() {
        return buildResourceStream(BORDER_HIT_SOUND);
    }
    /**
     * 
     * @return The input stream of the border hit sound
     */
    public static InputStream getBonusHitSoundStream() {
        return buildResourceStream(BONUS_HIT_SOUND);
    }
    /**
     * 
     * @return The input stream of the player gol sound
     */
    public static InputStream getPlayerGoalSoundStream() {
        return buildResourceStream(PLAYER_GOAL_SOUND);
    }
    /**
     * 
     * @return The input stream of the score history file
     */
    public static InputStream getScoreHistoryInputStream() { 
        try {
            return new FileInputStream(HOME_DIRECTORY + FILE_SEPARATOR + MATCH_SCORES);
        } catch (FileNotFoundException e) {
            return null;
        }
    }
    /**
     * 
     * @return The output stream of the score history file
     */
    public static OutputStream getScoreHistoryOutputStream() { 
        try {
            return new FileOutputStream(HOME_DIRECTORY + FILE_SEPARATOR + MATCH_SCORES);
        } catch (FileNotFoundException e) {
            return null;
        }
    }
    /**
     * 
     * @param obj The score which has to be saved.This method serializes the object argument
     */
    public static void saveScore(final Score obj) {
        new Serializer().saveScore(obj, getScoreHistoryInputStream());
    }
    /**
     * 
     * @return The scores saved previously
     */
    public static List<Score> getScoreHistory() {
        return new Serializer().getScoreHistory(getScoreHistoryInputStream());
    }

    /**
     *
     * @param stream The input stream of the file to be played
     */
    public static void playSound(final InputStream stream) {
        if (soundOn) {
            new SoundPlayer(SoundPlayer.loadSound(stream));
        }
    }
}
